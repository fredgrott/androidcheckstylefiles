AndroidCheckstyleFiles
===================

AndroidCheckstyleFiles, checkstyle file for android java application development
in Eclipse IDE format to be used by both ant and Eclipse IDE.

Project License
============

Apache License 2.0

Project Lead
==========

Fred Grott, his blogs:

[FredGrott-posterous](http://fredgrott.posterous.com)

[Shareme-githubpages](http://shareme.github.com)


